## Getting started
# glasgow

Glitch cLASsifier: Gws Or What?

### Installation

Requires just pytorch at the moment. I have not rigorously checked the python version. GstLAL related scripts will likely require a separate env.

```conda create -n <env> python=3.10```
For blip glitches, we will use ```gengli```. After dependencies are installed,
activate the environment and install directly with pip:

``` pip install gengli```

For scattering, we will use a parametric model: `https://git.ligo.org/rhiannon.udall/bilbyparametricglitch`. Clone this repo and install. 

Finally, install ```glasgow```

```pip install .```

Note that to use GPUs on CIT, you may need to export the appropriate flags. This can be done each time:

```export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/```

or you can add to your conda env's activation script:

```/home/albert.einstein/.conda/envs/<env>/etc/conda/activate.d/env_vars.sh```

There is also now a way to manage environment variables directly from the command line.


### Generating training data

1. Obtain Gaussian noise frames (```/home/gstlalcbc.offline/early_warning/frames/``` has O5 frames)
2. Inject either your signals or glitches into the frames
- a. Signals can be injected by generating a standard injection file and feeding through GstLAL
- b. Glitches can be made with gengli (blip) or Rhiannon's code (scattering). Here, we elect to rewrite the frames using ```scripts/add_to_frame.py```
3.   Make your desired template banks and decompose them.
4. Filter said data using ```gstlal_inspiral_ortho_output```. This will generate frame files that contain the orthogonal SNR time series.
- a. Signals are trivial -- just feed in the injection file + Gaussian frames.
- b. Glitches are dumb. Feed in the altered frames and a dummy injection file with the glitch times attached to signals that are too far away to recover.
5. Use ```process_injections``` to take out the snippets we care about and dump into ```.pt``` files in appropriately named directories.
6. Enjoy your new training data!
