import setuptools

with open("README.md", "r") as fh:
	long_description = fh.read()

install_requires = ['torch']

setuptools.setup(
	name = "glasgow",
	version = "0.0.1",
	author = "Ritwik Sharma, Ryan Magee, and friends",
	description = "Glitch cLASsifier: Gws Or What?",
	long_description = long_description,
	long_description_content_type = "text/markdown",
	url = "https://www.git.ligo.org/ritwik.sharma/glibh-svd-classifier",
	packages = ['glasgow'],
	zip_safe = False,
	classifiers = [
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: GPLv2",
		"Operating System :: OS Independent",
	],
	scripts = ['glasgow/bin/process_injections', 'glasgow/bin/plot_snr', 'glasgow/bin/plot_fft'],
	python_requires = '>=3.8',
	install_requires = install_requires
)
