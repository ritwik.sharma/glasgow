import torch
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
import numpy as np
from timeit import default_timer as timer
import sys
import os
import glob
import math
import time
from tqdm import tqdm
from torch import fft

# Dataset Class 

class CustomDataset(Dataset):

    def __init__(self, data_dir = 'pt_files', ifo = 'H1', classification = 0, trim = None, usefft = False, **kwargs):
        self.data_dir = data_dir
        self.ifo = ifo
        self.classification = torch.tensor([classification]).to(dtype=torch.float32)
        self.file_list = glob.glob(self.data_dir + "/*")
        self.usefft = usefft
        if trim:
            self.file_list = self.file_list[:trim]

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, index):
        data = torch.load(self.file_list[index])
        orthosnr = data['orthosnr']
        orthosnr = torch.dstack([orthosnr.real, orthosnr.imag]).view(orthosnr.shape[0], orthosnr.shape[1]*2)
        #if self.usefft:
        #    orthosnr = fft.fft(orthosnr)
        #    orthosnr = torch.dstack([orthosnr.real, orthosnr.imag]).view(orthosnr.shape[0], orthosnr.shape[1]*2)
        orthosnr = orthosnr.unsqueeze(dim=0)
        return orthosnr, self.classification, self.file_list[index]
    

# Helper functions

def my_collate(batch):

    batch = list(filter(lambda x: x is not None, batch))
    return torch.utils.data.dataloader.default_collate(batch)

def findConv2dOutShape(hin,win,conv,pool=(2,2)):

    # find how the input image dimensions change with every layer
    kernel_size=conv.kernel_size
    stride=conv.stride
    padding=conv.padding
    dilation=conv.dilation 

    hout=np.floor((hin+2*padding[0]-dilation[0]*(kernel_size[0]-1)-1)/stride[0]+1)
    wout=np.floor((win+2*padding[1]-dilation[1]*(kernel_size[1]-1)-1)/stride[1]+1)

    if pool:
        hout/=pool[0]
        wout/=pool[1]

    return int(hout),int(wout)


# Model Class

class ConvNet(nn.Module):

  # Model Architecture
    
    def __init__(self, input_channels=1, output_channels = 256, expansion = 2, dense_layer = 16, pool=(2,2), ker_size=(2,2), stride=1, pad=0):
        super(ConvNet, self).__init__()

        self.pool = pool
        self.ker_size = ker_size
        self.stride = stride
        self.pad = pad
        self.input_channels = input_channels
        self.output_channels = output_channels
        self.dense_layer = dense_layer
        self.expansion = expansion

        self.conv1 = nn.Conv2d(in_channels = self.input_channels, out_channels = self.output_channels, kernel_size = ker_size, stride = stride, padding = pad)
        self.h1, self.w1 = findConv2dOutShape(30, 201, self.conv1, pool = pool)
        self.h1, self.w1 = findConv2dOutShape(37, 402, self.conv1, pool = pool)

        self.dat = self.conv1.weight.data
        self.pool = nn.MaxPool2d(kernel_size = pool, stride=None)

        self.conv2 = nn.Conv2d(in_channels=self.output_channels, out_channels=self.output_channels * self.expansion, kernel_size=ker_size, stride=stride, padding=pad)
        self.h2, self.w2 = findConv2dOutShape(self.h1, self.w1, self.conv2, pool=pool)
        self.dat2 = self.conv2.weight.data

        self.conv3 = nn.Conv2d(in_channels=self.output_channels * self.expansion, out_channels=self.output_channels * self.expansion**2, kernel_size=ker_size, stride=stride, padding=pad)
        self.h3, self.w3 = findConv2dOutShape(self.h2, self.w2, self.conv3, pool=pool)

       
        self.dropout = nn.Dropout(0.5)
        #self.fc1 = nn.Linear(self.conv3.out_channels * self.h3 * self.w3, self.dense_layer)

        self.fc1 = nn.Linear(self.conv2.out_channels * self.h2 * self.w2, self.dense_layer)
        self.fc2 = nn.Linear(self.dense_layer, 1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):

        x = F.leaky_relu(self.pool(self.conv1(x)))
        x = F.leaky_relu(self.pool(self.conv2(x)))
        x = x.view(-1, self.conv2.out_channels*self.h2*self.w2)

        #x = F.relu(self.pool(self.conv3(x)))
        #x = x.view(-1, self.conv3.out_channels*self.h3*self.w3)

        x = F.leaky_relu(self.fc1(x))
        x = self.dropout(x)

        x = self.fc2(x)
        x = self.sigmoid(x)
        return x
    


# Training and Validation Class
    

class ModelTrainer():

    def __init__(self, model = ConvNet(), glitch_dir = "pt-files-glitches", signal_dir = "pt-files-signals", trim_length = None, batch_size = 16, do_shuffle = False, usefft = False, device = "cuda"):

        self.model = model
        self.glitch_dir = glitch_dir
        self.signal_dir = signal_dir
        self.trim = trim_length
        self.batch_size = batch_size
        self.shuffle = do_shuffle
        self.usefft = usefft
        self.device = device

    def prepare_datasets(self):

        glitch_data = CustomDataset(data_dir = self.glitch_dir, classification = 0, trim = self.trim, usefft = self.usefft)
        bbh_data = CustomDataset(data_dir = self.signal_dir, classification = 1, trim = self.trim, usefft = self.usefft)

        #for i in range(len(glitch_data)):
        #    glitch_data.__getitem__(i)
        #    bbh_data.__getitem__(i)

        all_data = torch.utils.data.ConcatDataset([glitch_data, bbh_data])

        train_split = 0.8
        val_split = 1 - train_split
        train_dataset, test_dataset = torch.utils.data.random_split(all_data, [math.floor(train_split*len(all_data)), math.ceil(val_split*len(all_data))])

        training_dataloader = DataLoader(train_dataset, batch_size = self.batch_size, shuffle = self.shuffle)
        testing_dataloader = DataLoader(test_dataset, batch_size = self.batch_size, shuffle = self.shuffle)
        return training_dataloader, testing_dataloader
    

    def train_model(self, train_dataset, test_dataset, learning_rate = 0.001, mnt = 0.9, epochs = 50):

        model = self.model

        device = self.device if torch.cuda.is_available() else "cpu"

        # Loss function
        criterion = torch.nn.BCELoss()

        # Optimizer
        optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate, momentum = mnt)
        optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)

        n_epochs = epochs
        model.to(device)

        valid_loss_min = np.Inf # track change in validation loss

        for _, epoch in enumerate(tqdm(range(1, n_epochs+1))):
            # keep track of training and validation loss
            train_loss = 0.0
            valid_loss = 0.0

            # train the model 
            model.train()
            for data, target, _ in train_dataset:
                train_data, train_target = data.to(device), target.to(device)
                # clear the gradients of all optimized variables
                optimizer.zero_grad()
                # forward pass: compute predicted outputs by passing inputs to the model               
                output = model(train_data)               
                # calculate the batch loss
                loss = criterion(output, train_target)
                # backward pass: compute gradient of the loss with respect to model parameters
                loss.backward()
                # perform a single optimization step (parameter update)
                optimizer.step()
                # update training loss
                train_loss += loss.item()*train_data.size(0)

            # Validating the model
            with torch.no_grad():
                model.eval()
                for data, target, _ in test_dataset:
                    data, target = data.to(device), target.to(device)
                    # forward pass: compute predicted outputs by passing inputs to the model
                    output = model(data)
                    # calculate the batch loss
                    loss = criterion(output, target)
                    # update average validation loss
                    valid_loss += loss.item()*data.size(0)

            # calculate average losses
            train_loss = train_loss/len(train_dataset)
            valid_loss = valid_loss/len(test_dataset)

            # print training/validation statistics
            print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
                epoch, train_loss, valid_loss))

            # save model if validation loss has decreased
            if valid_loss <= valid_loss_min:
                print('Validation loss decreased ({:.6f} --> {:.6f}).  Saving model ...'.format(
                valid_loss_min,
                valid_loss))
                torch.save(model.state_dict(), 'model.pt')
                valid_loss_min = valid_loss

        self.model = model


    def load_model(self, model_dir = "model.pt"):

        model = self.model
        device = self.device if torch.cuda.is_available() else "cpu"

        model.load_state_dict(torch.load(model_dir, map_location=torch.device(device)))
        self.model = model


    def eval_model(self, test_data, model_dir = "model.pt"):

        model = self.model
        device = self.device if torch.cuda.is_available() else "cpu"
        ins = []
        outs = []
        model.eval()
        with torch.no_grad():
            length = 0
            total_mismatch = 0
            for images, labels, filenames in test_data:
                images = images.to(device)
                labels = labels.to(device)
                outputs = model(images)
                print(outputs.data.cpu(), labels.data.cpu())
                diff = torch.abs((torch.round(outputs.data)-labels.data)).flatten()
                mismatch = torch.sum(diff)
                total_mismatch += mismatch
                length += len(labels.flatten())
                outs.extend(list(outputs.data.cpu().flatten()))
                ins.extend(list(labels.data.cpu().flatten()))
                mask = (diff.cpu().numpy() == 1).squeeze()
                #if mismatch > 0.:
                #    print(np.array(filenames)[mask], outputs.data.cpu().numpy()[mask], labels.data.cpu().numpy()[mask])

            print('Accuracy of the network on the {} testing images: {} %'.format(length, 100 * (length-total_mismatch) / length))
        np.savetxt('outs.txt',np.array(outs))
        np.savetxt('ins.txt', np.array(ins))



        




