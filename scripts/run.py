import torch
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
import numpy as np
from timeit import default_timer as timer
from tqdm import tqdm

from glasgow.model import ConvNet
from glasgow.model import ModelTrainer


# Custom Model 

model = ConvNet(input_channels=1, output_channels = 64, expansion = 2, dense_layer = 16, pool=(2,2), ker_size=(7,7), stride=1, pad=0)

# Model Trainer called with default arguments

trainer = ModelTrainer(model = model, glitch_dir = '../scattering/pt-scattering/', signal_dir = '../glitch/pt-blips/', trim_length = None)
training_data, testing_data = trainer.prepare_datasets()


# Steps called for training, loading and evaluating the model

trainer.train_model(train_dataset = training_data, test_dataset = testing_data, epochs = 50)
trainer.load_model()
trainer.eval_model(test_data = testing_data)

