import copy
from tqdm import tqdm
from gwpy.timeseries import TimeSeries
from gwpy.frequencyseries import FrequencySeries

import bilby
import bilby_glitch

from typing import Tuple, Optional

import logging

import logging


class DisableLogger:
    # https://stackoverflow.com/questions/2266646/how-to-disable-logging-on-the-standard-error-stream
    def __enter__(self):
        logging.disable(logging.CRITICAL)

    def __exit__(self, exit_type, exit_value, exit_traceback):
        logging.disable(logging.NOTSET)


def inject_scaled_samples(
    ifo_name: str,
    timeseries: TimeSeries,
    power_spectral_density_file: str,
    prior_file: str,
    first_injection_buffer_time_in_seconds: Optional[float] = 1000,
    last_injection_buffer_time_in_seconds: Optional[float] = 1000,
    injections_time_spacing_in_seconds: Optional[float] = 20,
    number_of_injections: Optional[int] = 1000,
    target_snr: Optional[float] = 10,
) -> TimeSeries:
    """Injects a series of glitches from the given prior, scaled to a target SNR

    Parameters
    ==========
    ifo_name : str
        The name of the ifo to inject into (used to determine ifo geometry)
    timeseries : TimeSeries
        The (noise) timeseries to set the ifo data from
    power_spectral_density_file : str
        A file encoding the PSD corresponding to the data
    prior_file : str
        A file with the prior that should be used to generate the arches.
        To exclude arches set the prior on their amplitude to 0
    first_injection_buffer_time : Optional[float]
        The amount of buffer time in seconds to leave before the first injections
    last_injection_buffer_time_in_seconds : Optional[float]
        The amount of buffer time in seconds to leave after the last injections
    injections_time_spacing_in_seconds : Optional[float]
        The amount of time in seconds to leave between injections.
        Slow scattering glitchs may have width of 1 / 2 / f_{mod}
        (in principle - pragmatically somewhat less due to the noise curve)
        So this should be greater than or equal to 1 / 2 / min(f_{mod})
    number_of_injections: Optional[int],
        The maximum number of injections to perform
        If this (in combination with the above parameters)
        would exceed the amount of time in the data, then the maximum
        possible number of injections will be determined and these will
        be injected.
    target_snr: Optional[float]
        The SNR to scale each injection to. Scaling is uniform over each arch amplitude.

    Returns
    =======
    TimeSeries
        The gwpy timeseries with glitches injected
    """
    if ".xml.gz" in power_spectral_density_file:
        power_spectral_density = FrequencySeries.read(
            power_spectral_density_file, instrument=ifo_name
        )
        power_spectral_density = bilby.gw.detector.PowerSpectralDensity(
            frequency_array=power_spectral_density.frequencies,
            psd_array=power_spectral_density.value,
        )
    else:
        power_spectral_density = (
            bilby.gw.detector.PowerSpectralDensity.from_power_spectral_density_file(
                power_spectral_density_file
            )
        )

    ifo, waveform_generator, prior = construct_bilby_required_objects(
        ifo_name=ifo_name,
        timeseries=timeseries,
        power_spectral_density=power_spectral_density,
        prior_file=prior_file,
    )

    total_time_needed = (
        first_injection_buffer_time_in_seconds
        + last_injection_buffer_time_in_seconds
        + (number_of_injections - 1) * injections_time_spacing_in_seconds
    )
    if total_time_needed > ifo.duration:
        print(
            "Insufficient time in data to inject all requested glitches at requested intervales"
        )
        number_of_injections = (
            int(
                (
                    ifo.duration
                    - first_injection_buffer_time_in_seconds
                    - last_injection_buffer_time_in_seconds
                )
                / injections_time_spacing_in_seconds
            )
            + 1
        )
        print(f"Will inject {number_of_injections} (maximum allowable) instead")

    with DisableLogger():
        for ii in tqdm(range(number_of_injections)):
            inject_scaled_sample(
                ifo=ifo,
                central_time=ifo.start_time
                + first_injection_buffer_time_in_seconds
                + ii * injections_time_spacing_in_seconds,
                waveform_generator=waveform_generator,
                prior=prior,
                target_snr=target_snr,
            )
    return ifo.strain_data.to_gwpy_timeseries()


def construct_bilby_required_objects(
    ifo_name: str,
    timeseries: TimeSeries,
    power_spectral_density: str,
    prior_file: str,
) -> Tuple[
    bilby.gw.detector.interferometer.Interferometer,
    bilby.gw.waveform_generator.WaveformGenerator,
    bilby.core.prior.PriorDict,
]:
    """Builds analysis components from inputs

    Parameters
    ==========
    ifo_name : str
        The name of the ifo to inject into (used to determine ifo geometry)
    timeseries : TimeSeries
        The (noise) timeseries to set the ifo data from
    power_spectral_density_file : str
        A file encoding the PSD corresponding to the data
    prior_file : str
        A file with the prior that should be used to generate the arches.
        To exclude arches set the prior on their amplitude to 0

    Returns
    =======
    bilby.gw.detector.interferometer.Interferometer
        The inteferometer object associated with the strain data
    bilby.gw.waveform_generator.WaveformGenerator
        The waveform generator for the glitch
    bilby.core.prior.PriorDict
        The prior to draw glitch configurations from (before rescaling)
    """
    ifo = bilby.gw.detector.get_empty_interferometer(ifo_name)
    ifo.power_spectral_density = power_spectral_density
    ifo.set_strain_data_from_gwpy_timeseries(timeseries)
    waveform_generator = bilby_glitch.waveform_generator.GlitchWaveformGenerator(
        glitch_index_name="slow",
        glitch_interferometer=ifo_name,
        duration=ifo.duration,
        sampling_frequency=ifo.sampling_frequency,
        start_time=ifo.strain_data.start_time,
        frequency_domain_source_model=bilby_glitch.glitch_models.slow_scattering,
        parameter_conversion=None,
        direct_injection=True,
        waveform_arguments={"centered_arches": True, "number_harmonics": 10},
    )

    prior_dict = bilby.core.prior.PriorDict(prior_file)

    return ifo, waveform_generator, prior_dict


def inject_scaled_sample(
    target_snr: float,
    central_time: float,
    prior: bilby.core.prior.PriorDict,
    waveform_generator: bilby.gw.waveform_generator.WaveformGenerator,
    ifo: bilby.gw.detector.Interferometer,
):
    """Draw a sample from the prior then scale the amplitudes to make the glitch a certain SNR

    Parameters
    ==========
    target_snr : float
        The SNR to scale to
    central_time : float
        The central time of the injected glitch
    prior : `bilby.core.prior.PriorDict`, optional
        The prior to draw the initial samples from
    ifos : bilby.gw.detector.Interferometer, optional
        An IFO to inject into

    Returns
    =======
    dict
        The parameters, rescaled to the desired SNR
    dict
        The original parameters before rescaling
    """
    initial_sample = prior.sample(1)
    initial_sample = {k: v[0] for k, v in initial_sample.items()}
    initial_sample = bilby_glitch.glitch_models.populate_unused_parameters(
        initial_sample, add_sky_params=True
    )
    initial_sample["geocent_time"] = central_time
    rescaled_sample = copy.deepcopy(initial_sample)
    waveform_generator.waveform_arguments["post_trigger_duration"] = (
        ifo.start_time + ifo.duration - central_time
    )
    temp_ifo = copy.deepcopy(ifo)
    temp_ifo.inject_signal_from_waveform_generator(
        waveform_generator=waveform_generator,
        parameters=initial_sample,
    )

    optimal_snr = temp_ifo.meta_data["optimal_SNR"]
    snr_ratio = target_snr / optimal_snr

    for key, val in initial_sample.items():
        if "amp" in key:
            rescaled_sample[key] = val * snr_ratio

    ifo.inject_signal_from_waveform_generator(
        waveform_generator=waveform_generator,
        parameters=rescaled_sample,
    )

    return rescaled_sample, initial_sample
